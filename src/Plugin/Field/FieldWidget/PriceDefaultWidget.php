<?php

namespace Drupal\price_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\NumberWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'price' widget.
 *
 * @FieldWidget(
 *   id = "price_default_widget",
 *   label = @Translation("Hidden (Automatic)"),
 *   field_types = {
 *     "price"
 *   }
 * )
 */
class PriceDefaultWidget extends NumberWidget
{

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
  {
    return parent::formElement($items, $delta, $element, $form, $form_state);
  }
}
