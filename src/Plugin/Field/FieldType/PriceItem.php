<?php

namespace Drupal\price_field\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\DecimalItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
//use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Core\Entity\Sql\DefaultTableMapping;


/**
 * Plugin implementation of the 'Price' field type.
 *
 * @FieldType(
 *   id = "price",
 *   label = @Translation("Price Field"),
 *   module = "field_example",
 *   description = @Translation("Field type to input Prices + Currencies."),
 *   default_widget = "price_default_widget",
 *   default_formatter = "price_default_formatter"
 * )
 */
class PriceItem extends DecimalItem
{
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition)
  {
    $schema = parent::schema($field_definition);
    $schema['colums']['currency'] = [
      'type' => 'string',
      'not null' => TRUE,
      'length' => 3,
      'description' => 'The ISO currency code (external referenc3e to the Currency module entity)',
    ];

    if(!array_key_exists('indexes', $schema))
    {
      $schema['indexes'] = [];
    }

    $schema['indexes']['currency'] = ['currency'];

    if(!array_key_exists('foreign keys', $schema))
    {
      $schema['foreign keys'] = [];
    }

    $schema['foreign keys']['currency'] = [
      'table' => 'currencies',
      'columns' => [
        'currency' => 'cid',
      ]
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition)
  {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['currency'] = DataDefinition::create('string')
      ->setLabel(t('The pattern used for creating the auto grouping.'))
      ->setRequired(TRUE);

    return $properties;
  }
}
